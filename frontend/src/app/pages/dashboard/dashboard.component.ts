import {Component, OnInit} from '@angular/core';
import {DashboardService} from './dashboard.service';
import {HttpClient} from '@angular/common/http';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {BASILICA, PARK_GUELL, MUSEO_ARTE, SAGRADA_FAMILIA, TEATRO_LICEU} from './places.constant.';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

@Component({
  selector: 'dashboard-cmp',
  moduleId: module.id,
  templateUrl: 'dashboard.component.html'
})

export class DashboardComponent implements OnInit {

  mapHtmlUrl: SafeResourceUrl;
  optionSelected = '';
  loading = false;

  myControl = new FormControl();
  options: string[] = [SAGRADA_FAMILIA, PARK_GUELL, TEATRO_LICEU, MUSEO_ARTE, BASILICA];
  filteredOptions: Observable<string[]>;

  constructor(private _httpClient: HttpClient, public sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
    this.mapHtmlUrl = this.sanitizer.bypassSecurityTrustResourceUrl('assets/maps/empty.html');
  }

  onOptionSelected(ev: any) {
    this.loading = true;
    this.optionSelected = ev.option.value
    console.log(this.optionSelected)
    switch (this.optionSelected) {
      case SAGRADA_FAMILIA:
        this.pickMap('sagrada')
        break;
      case PARK_GUELL:
        this.pickMap('dorta')
        break;
      case TEATRO_LICEU:
        this.pickMap('liceu')
        break;
      case MUSEO_ARTE:
        this.pickMap('museo')
        break;
      case BASILICA:
        this.pickMap('basilica')
        break;
      default:
        break;
    }
  }

  pickMap(mapName: string) {
    setTimeout(() => {
      this.loading = false;
    }, 5000);
    this.mapHtmlUrl = this.sanitizer.bypassSecurityTrustResourceUrl('assets/maps/' + mapName + '.html');
  }

  onModelChange() {
    this.mapHtmlUrl = this.sanitizer.bypassSecurityTrustResourceUrl('assets/maps/empty.html');
  }


  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }
}
